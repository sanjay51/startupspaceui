startupSpaceApp.service('Constants', function($rootScope) {
    this.user_logged_in = "USER_LOGGED_IN";
    this.user_logged_out = "USER_LOGGED_OUT";

    this.FIREBASE_URL = "https://glowing-inferno-3206.firebaseio.com";
});
