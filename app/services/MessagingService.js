'use strict';

startupSpaceApp.factory("MessagingService", ["$firebaseObject", "$firebaseArray", "Auth", "Constants", "BroadcastService", "User",
    function($firebaseObject, $firebaseArray, Auth, Constants, BroadcastService, User) {
        var service = {};

        service.getAllConversations = function(userID) {
            var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/users/" + userID + "/conversations");
            return ref;
        };

        service.getConversationByID = function(convID) {
            var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/conversations/" + convID);
            return $firebaseObject(ref);
        };

        service.getConversationMetadata = function(convID) {
            var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/conversations/" + convID + "/metadata");
            return $firebaseObject(ref);
        };

        service.addMessage = function(convID, messageBody, author) {
            if (!messageBody || !author) {
                return;
            }

            var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/conversations/" + convID + "/messages");
            var messagesArray =  $firebaseArray(ref);
            return messagesArray.$add({ author: author, body: messageBody });
        }

        service.setUnreadConvCount = function(scope) {
            scope.unreadConvCount = 0;
            var ref = new Firebase(Constants.FIREBASE_URL + "/users/" + User.uid + "/conversations");
            ref.on('child_added', function(conversation) {
                var promise = service.getConversationMetadata(conversation.key());
                promise.$loaded().then(function(metadata) {
                    if (!metadata.read_status || !metadata.read_status[User.uid]) {
                        scope.unreadConvCount++;
                    }
                });
            });

        }

        service.markConversationAsRead = function(convID) {
            var url = Constants.FIREBASE_URL + "/conversations/" + convID + "/metadata/read_status";
            var ref = new Firebase(url);
            var obj = $firebaseObject(ref);
            obj.$loaded().then(function(data) {
                obj[User.uid] = true;
                obj.$save();
            });
        }

        service.markConversationAsNotRead = function(convID) {
            var url = Constants.FIREBASE_URL + "/conversations/" + convID + "/metadata/read_status";
            var ref = new Firebase(url);
            var obj = $firebaseObject(ref);

            //this will overwrite the current object
            obj[User.uid] = true;
            obj.$save();
        }

        return service;
    }
]);
