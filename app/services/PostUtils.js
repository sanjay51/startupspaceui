'use strict';

startupSpaceApp.factory('PostUtils', function($rootScope, $firebaseArray, $firebaseObject) {
    var posts = {};

    posts.getAllPosts = function() {
        return posts.getPostBase();
    };

    posts.createPost = function(title, description, author) {
        console.log("Posting title: " + title + " | description: " + description + " | author: " + author);
        return posts.getPostBase().$add({ title: title, description: description, author: author});
    };

    posts.getPostBase = function() {
        var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/posts");
        return $firebaseArray(ref);
    };

    posts.getPostByID = function(postID) {
        var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/posts/" + postID);
        return $firebaseObject(ref);
    };

    posts.addCommentToPost = function(userID, postID, comment) {
        if (!comment) {
            return;
        }

        var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/posts/" + postID + "/comments");
        var commentsArray =  $firebaseArray(ref);
        return commentsArray.$add({ author: userID, data: comment});
    };

    posts.getPostsFromRemoteArray = function(firebaseArray) {
        var newPostArray = [];
        angular.forEach(firebaseArray, function(post) {
            var newPost = {
                id: post.$id,
                title : post.title,
                description : post.description,
                author : post.author,
                comments : []
            };

            angular.forEach(post.comments, function(comment) {
                newPost.comments.push({
                    author : comment.author,
                    data : comment.data
                });
            });

            newPostArray.push(newPost);
        });

        return newPostArray;
    };

    return posts;
});
