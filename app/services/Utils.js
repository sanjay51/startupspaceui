startupSpaceApp.service('Utils', function() {
    this.RESPONSE_START = "[RESPONSE_START]";
    this.RESPONSE_END = "[RESPONSE_END]";
    this.ERROR = "[ERROR]";

    this.getActualResponse = function(str) {
        str = str.substring(str.indexOf(this.RESPONSE_START) + this.RESPONSE_START.length, str.indexOf(this.RESPONSE_END));
        str = str.substring(str.indexOf(this.ERROR) + this.ERROR.length);

        return this.getPrettyPrint(str);
    };

    this.getPrettyPrint = function(str) {
        return  "- " + str.split(".").join(".<br/> - ").slice(0, -2);
    };

    this.isErrorResponse = function(str) {
        return str.indexOf(this.ERROR) > -1;
    };

    this.getErrorMessage = function (error) {
        var message = "Something went wrong. ";

        switch(error.code) {
            case "INVALID_EMAIL":
                message = "Invalid email";
                break;
            case "INVALID_PASSWORD":
                message = "Invalid password";
                break;
            case "INVALID_USER":
                message = "The user account doesn't exist.";
                break;
            case "EMAIL_TAKEN":
                message = "Account already exists with this email.";
                break;
            default:
                message = "Something went wrong. " + error;
        }

        return message;
    };
});
