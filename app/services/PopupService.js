startupSpaceApp.service('PopupService', function() {
    this.alertPopup = {};
    this.alertPopup.title = "some title";
    this.alertPopup.message = "";
    this.alertPopup.callback =  function () {};
    this.alertPopup.buttonText = "";

    this.showAlert = function(title, message, buttonText, callback) {
        this.alertPopup.title = title;
        this.alertPopup.message = message;
        this.alertPopup.buttonText = buttonText;
        this.alertPopup.callback = callback;

        $('#alertModal').modal('show');
    };
});
