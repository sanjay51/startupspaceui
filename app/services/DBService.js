startupSpaceApp.service('DBService', function($http) {

    this.loadWorkspace = function(id) {
        return $http.get('db/loadWorkspace.php?id=' + id);
    };

    this.performSignUp = function(userData, successCallback, failureCallback) {
        $http.post('php/db/signUp.php', userData)
            .success(function(response) {
                successCallback(response);
            })
            .error(function(response) {
                failureCallback(response);
            });

    };

    this.ok = function() {
        alert('DBService OK');
    };
});
