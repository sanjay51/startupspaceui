startupSpaceApp.service('Validation', function() {

    this.validateEmail = function(email) {
        if (email) {
            email = email.trim();
        }

        if (!email) {
            return "<br/>- Email cannot be blank.";
        }

        var filter = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;

        if (!filter.test(email)) {
            return "<br/>- Email is not valid.";
        }

        return "";
    };

    this.validatePassword = function(password) {
        if (password) {
            password = password.trim();
        }

        if (!password) {
            return "<br/>- Password cannot be blank.";
        }

        if (password.length < 7) {
            return "<br/>- Password should contain atleast 7 characters.";
        }

        if (password.length > 255) {
            return "<br/>- Password cannot contain more than 255 characters";
        }

        return "";
    };

    this.validateName = function(name) {
        if (name) {
            name = name.trim();
        }

        if (!name) {
            return "<br/>- Name cannot be blank.";
        }

        if (name.length < 2) {
            return "<br/>- Name is too short.";
        }

        if (name.length > 255) {
            return "<br/>- Name cannot contain more than 255 characters";
        }

        return "";
    };

    this.validateTitle = function(title) {
        if (title) {
            title = title.trim();
        }

        if (!title) {
            return "<br/>- Title cannot be blank.";
        }

        if (title.length < 10) {
            return "<br/>- Title should contain atleast 10 characters.";
        }

        if (title.length > 1000) {
            return "<br/>- Title cannot contain more than 1000 characters.";
        }

        return "";
    };

    this.validateDescription = function(description) {
        if (description) {
            description = description.trim();
        }

        if (!description) {
            return "<br/>- Description cannot be blank.";
        }

        if (description.length < 10) {
            return "<br/>- Description should contain atleast 10 characters.";
        }

        if (description.length > 10000) {
            return "<br/>- Title cannot contain more than 10,000 characters.";
        }

        return "";
    };
});
