'use strict';

var testApp = angular.module('testApp', ["firebase"]);

testApp.factory("Auth", ["$firebaseAuth",
    function($firebaseAuth) {
        var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com");
        return $firebaseAuth(ref);
    }
]);

testApp.factory("User", ["$firebaseObject",
    function($firebaseObject) {
        var user = {};
        user.uid = null;
        user.object = null;

        user.getUser = function() {
            if (user.uid === null) {
                return null;
            } else {
                if (user.object === null) {
                    var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/users/" + user.uid);
                    user.object = $firebaseObject(ref);
                }

                return user.object;
            }
        };

        user.setUID = function(uid) {
            user.uid = uid;
        };

        user.save = function() {
            user.object.$save().then(function(ref) {
                console.log("saved " + ref);
            }, function(error) {
                console.log(error);
            });
        };

        return user;
    }
]);

testApp.factory("PostUtils", ["$firebaseArray",
    function($firebaseArray) {
        var post = {};
        post.getPosts = function() {
            var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/posts");
            return $firebaseArray(ref);
        };

        return post;
    }
]);

testApp.controller("FirebaseController", function($scope, Auth, User, PostUtils) {
    $scope.email = "sanjay@gmail.com";
    $scope.password = "mypassword";
    $scope.uid = "";

    $scope.createUser = function() {
        Auth.$createUser({
            email: $scope.email,
            password: $scope.password
        }).then(function(userData) {
            console.log(userData);
        }).catch(function(error) {
            console.log(error);
        });
    };

    $scope.login = function() {
        Auth.$authWithPassword({
            email: $scope.email,
            password: $scope.password
        }).then(function(authData) {
            User.setUID(authData.uid);
            console.log(authData);
        }).catch(function(error) {
            console.log(error);
        });
    };

    $scope.write = function() {
        var obj = {
            obj: "obj",
            x: "x"
        };
        User.getUser().data = obj;
        User.save();
    };

    $scope.writeToArray = function(title, description) {
        PostUtils.getPosts().$add({ title: title, description: description});
    };

    $scope.logout = function() {
        Auth.$unauth();
    };

    Auth.$onAuth(function(authData) {
        if (authData) {
            console.log("Logged in as:", authData.uid);
        } else {
            console.log("Logged out");
        }
    });
});


testApp.controller("FirebaseController1", function($scope, $firebaseObject) {
    var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com");

    var syncObject = $firebaseObject(ref);
    syncObject.$bindTo($scope, "data");

    $scope.perform = function() {
        $scope.data.sanjay = "jjj";
    };

});
