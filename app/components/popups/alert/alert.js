'use strict';

startupSpaceApp.controller('alertPopupController', function($scope, PopupService) {
    $scope.title = "";
    $scope.message = "";
    $scope.buttonText = "Ok";
    $scope.callback = function () {};

    $scope.$watch(
        function() {
            var title = PopupService.alertPopup.title;
            var message = PopupService.alertPopup.message;
            var buttonText = PopupService.alertPopup.buttonText;
            var callback = PopupService.alertPopup.callback;
            return title + message + buttonText + callback;
        }, function() {
            $scope.title = PopupService.alertPopup.title;
            $scope.message = PopupService.alertPopup.message;
            $scope.buttonText = PopupService.alertPopup.buttonText;
            $scope.callback = PopupService.alertPopup.callback;
        });

    $scope._click_button = function() {
        $('#alertModal').modal('hide');
        $scope.callback();
    };

});

startupSpaceApp.directive('alertPopup', function() {
    return {
        restrict: 'E',
        templateUrl: 'components/popups/alert/alert.html',
        scope: {
            postData: '='
        },
        controller: 'alertPopupController'
    };
});

startupSpaceApp.filter('to_trusted_html', ['$sce',
    function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    }
]);
