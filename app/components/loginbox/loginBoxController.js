'use strict';

startupSpaceApp.controller('LoginBoxController', function($scope, User) {
    $scope.showWarning = false;
    $scope.isLoggingIn = false;
    $scope.warnMessage = "";

    $scope.email = "";
    $scope.password = "";

    //Try to log in
    var authData = User.getAuth();
    if (authData) {
        User.setData(authData);
        User.loadProfile();
    }

    $scope._click_signup = function() {
        $location.path('#/signup');
    };

    $scope.login = function() {
        $scope.showWarning = false;
        var validationOutput = $scope.validateInput();

        if (validationOutput) {
            $scope.showWarning = true;
            $scope.warnMessage = validationOutput;
            return;
        }

        User.logout();
        $scope.isLoggingIn = true;

        var promise = User.login($scope.email, $scope.password);
        promise.then(function(authData) {
            User.setData(authData);
            $scope.loadProfile();
            console.log(authData);
        }).catch(function(error) {
            $scope.showWarning = true;

            switch(error.code) {
                case "INVALID_EMAIL":
                    $scope.warnMessage = "Invalid email";
                    break;
                case "INVALID_PASSWORD":
                    $scope.warnMessage = "Invalid password";
                    break;
                case "INVALID_USER":
                    $scope.warnMessage = "The user account doesn't exist.";
                    break;
                default:
                    $scope.warnMessage = ":" + error;
            }
        }).finally(function() {
            $scope.isLoggingIn = false;
        });


    };

    $scope.loadProfile = function() {
        User.loadProfile();
    };

    $scope.validateInput = function() {
        if (! $scope.email) {
            return "Email cannot be blank";
        }

        if (! $scope.password) {
            return "Password cannot be blank";
        }

        return null;
    };
});

startupSpaceApp.directive('ssLoginBox', function() {
    return {
        restrict: 'E',
        templateUrl: 'components/loginbox/loginBox.html',
        scope: {
            postData: '='
        },
        controller: 'LoginBoxController'
    };
});
