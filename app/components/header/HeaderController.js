'use strict';

startupSpaceApp.controller('HeaderController', function($scope, User, Constants, MessagingService) {
    $scope.isLoggedIn = false;
    $scope.email = User.email;
    $scope.name = User.name;
    $scope.profileImage = "";

    $scope.unreadConvCount = 0;

    $scope.$watch(function() {
        return User.email + User.name + User.image;
    }, function() {
        if (User.email !== "") {
            $('#loginModal').modal('hide');
            $scope.isLoggedIn = true;
            $scope.email = User.email;
            $scope.name = User.name;
            $scope.profileImage = User.image;
        }
    });

    $scope.logout = function() {
        User.logout();
        $scope.isLoggedIn = false;
        $scope.email = User.email;
    };


    $scope.$on(Constants.user_logged_in, function() {
        $scope.isLoggedIn = true;
        MessagingService.setUnreadConvCount($scope);
    });
});

startupSpaceApp.directive('ssHeader', function() {
    return {
        restrict: 'E',
        templateUrl: 'components/header/header.html',
        scope: {
            postData: '='
        },
        controller: 'HeaderController'
    };
});
