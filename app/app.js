'use strict';

var startupSpaceApp = angular.module('startupSpaceApp', ['ngResource', 'ngRoute', 'firebase']);

startupSpaceApp.config(function($routeProvider) {

  $routeProvider.
      when('/', {
        templateUrl: 'views/home.html'
      }).
      when('/messages', {
        templateUrl: 'views/messages.html'
      }).
      when('/signup', {
        templateUrl: 'views/signupview.html'
      }).
      when('/createpost', {
        controller: 'CreatePostController',
        templateUrl: 'views/createpost.html'
      }).
      when('/post/:postid', {
        controller: 'SinglePostController',
        templateUrl: 'views/post.html'
      }).
      when('/who-we-are', {
        templateUrl: 'views/login.html'
      }).
      when('/how-it-works', {
        templateUrl: 'views/login.html'
      }).
      when('/help', {
        templateUrl: 'views/help.html'
      }).
      when('/about', {
        templateUrl: 'views/about.html'
      }).
      when('/credits', {
        templateUrl: 'views/credits.html'
      }).
      when('/contact', {
        templateUrl: 'views/contact.html'
      });
});
