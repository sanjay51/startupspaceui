'use strict';

startupSpaceApp.controller('PostsController',
    function($scope, PostUtils, User) {

        $scope.posts = {};

        $scope.isLoggedIn = true;

        var posts = PostUtils.getAllPosts();
        posts.$loaded(function() {
            $scope.posts = PostUtils.getPostsFromRemoteArray(posts);

            angular.forEach($scope.posts, function(post) {
                if (post.author) {
                    var userObject = User.getUserByID(post.author);

                    userObject.$loaded().then(function() {
                        post.author_name = userObject.name;
                        post.author_image = userObject.image;

                        angular.forEach(post.comments, function(comment) {
                            if (comment.author) {
                                var commentUserObject = User.getUserByID(comment.author);
                                commentUserObject.$loaded().then(function() {
                                    comment.author_name = commentUserObject.name;
                                    comment.author_image = commentUserObject.image;
                                });
                            }
                        });
                    });
                }
            });
        });
    });
