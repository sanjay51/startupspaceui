'use strict';

startupSpaceApp.controller('SinglePostController',
    function($scope, $routeParams, PostUtils, User, Constants) {
        $scope.postID = $routeParams.postid;
        $scope.post = {};
        $scope.comment_image = "https://scontent-arn2-1.xx.fbcdn.net/hprofile-xtp1/v/t1.0-1/c27.0.160.160/p160x160/10955740_392474070922603_728193912882337259_n.jpg?oh=c891ef6e4fd5b9d4dace9253c999a399&oe=56E40694";
        $scope.isLoggedIn = User.isLoggedIn();
        $scope.isAddingComment = false;

        if ($scope.postID) {
            var postObject = PostUtils.getPostByID($scope.postID);

            postObject.$loaded().then(function() {
                console.log(postObject.$id + "  " + postObject.author);
                $scope.post.post_id = postObject.$id;
                $scope.post.title = postObject.title;
                $scope.post.description = postObject.description;

                var userObject = User.getUserByID(postObject.author);

                userObject.$loaded().then(function() {
                    console.log(userObject);
                    $scope.post.author_name = userObject.name;
                    if (userObject.image) {
                        $scope.post.author_image = userObject.image;
                    } else {
                        $scope.post.author_image = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xtf1/v/t1.0-1/c6.0.160.160/p160x160/12027823_1063862410304950_2254239607122516916_n.jpg?oh=a451ceee306e274187f0033844adf679&oe=56F388E8&__gda__=1459011139_31433c8add8be16c035d17b831b337df";
                    }
                });
            });
        }

        $scope.$on(Constants.user_logged_in, function() {
            $scope.isLoggedIn = true;
        });

        $scope.$on(Constants.user_logged_out, function() {
            $scope.isLoggedIn = false;
        });

        $scope._key_press = function(keyEvent) {
            if (keyEvent.keyCode != 13) {
                return;
            }

            keyEvent.preventDefault();

            $scope.isAddingComment = true;

            PostUtils.addCommentToPost(User.uid, $scope.postData.id, $scope.postData.newcomment)
                .then(function() {
                    $scope.postData.comments.push({
                        author: User.uid,
                        data: $scope.postData.newcomment,
                        author_name: User.name,
                        author_image: User.image
                    });

                    $scope.postData.newcomment = "";
                }).catch(function(error) {
                    console.log("error in commenting : " + error);
                }).finally(function() {
                    $scope.isAddingComment = false;
                });
        };
    });
