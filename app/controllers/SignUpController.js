'use strict';

startupSpaceApp.controller('SignUpController', function($scope, $location, DBService, PopupService, Utils, User, Validation) {
    $scope.showWarning = false;
    $scope.warnMessage = "";

    $scope.signupObject = {};
    $scope.signupObject.email = "sanjay.verma.nitk@gmail.com";
    $scope.signupObject.password = "haryana";
    $scope.signupObject.confirm_password = "haryana";
    $scope.signupObject.name = "Sanjay V";
    $scope.signupObject.type = "individual";

    $scope.isSigningUp = false;

    $scope._click_cancel = function() {
        $location.path('/');
    };

    $scope.successCallback = function(response) {
        var successCallback = function() {
            $location.path('/');
            $('#loginModal').modal('show');
        };

        var errorCallback = function() {};

        if (Utils.isErrorResponse(response)) {
            PopupService.showAlert('Please fix these:', Utils.getActualResponse(response), "Go back", errorCallback);
        } else {
            PopupService.showAlert('Success', "You have signed up successfully. Hit 'Next' to login.", "Next", successCallback);
        }

    };

    $scope.signupSuccess = function() {
        $location.path('/');
        $('#loginModal').modal('show');
    }

    $scope.failureCallback = function(response) {
        alert("response");
    };

    $scope.signup = function() {
        $scope.showWarning = false;
        var validationOutput = $scope.validateInput();

        if (validationOutput !== "") {
            $scope.showWarning = true;
            $scope.warnMessage = validationOutput;
            return;
        }

        $scope.isSigningUp = true;

        User.logout();
        var promise = User.signup($scope.signupObject.email, $scope.signupObject.password);

        promise.then(function(authData) {
            User.setUID(authData.uid);
            $scope.createProfile();
            console.log(authData);
        }).catch(function(error) {
            $scope.showWarning = true;
            $scope.warnMessage = Utils.getErrorMessage(error);
        }).finally(function() {
            $scope.isSigningUp = false;
        });
    };

    $scope.createProfile = function() {
        //create profile
        $scope.isSigningUp = true;

        var promise = User.createProfile($scope.signupObject.name, $scope.signupObject.type);
        promise.then(function(ref) {
            console.log(ref);
            $scope.signupSuccess();
        }, function(error) {
            console.log("Error", error);
        });

        $scope.isSigningUp = false;
    };

    $scope.validateInput = function() {
        var message = "";

        message += Validation.validateEmail($scope.signupObject.email);
        message += Validation.validatePassword($scope.signupObject.password);

        if ($scope.signupObject.confirm_password != $scope.signupObject.password) {
            message += "<br/>- Password do not match.";
        }

        message += Validation.validateName($scope.signupObject.name);

        if (!$scope.signupObject.type) {
            message += "<br/>- Please select a user type.";
        }

        return message;
    };

});
