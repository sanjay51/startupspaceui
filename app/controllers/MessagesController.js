'use strict';

startupSpaceApp.controller('MessagesController', function($scope, $location, Constants, Utils, User, MessagingService) {
    $scope.conversations = [];
    $scope.conversation = {};
    $scope.conversation.messages = [];

    $scope.isAddingMessage = false;
    $scope.selectedConvIndex = 0;
    $scope.newMessage = {};
    $scope.newMessage.data = "";

    /*
    -- load the conversations --
    $scope.conversations = [
        {
            partners: [{id: "swebdev1" }],
            unread_count: 4,
            title: "Regarding opportunity"
        },
        ....
    ]

    $scope.conversation = {
        messages: [{
            author: "zxcv",
            body: "hi this is zxcv"
        },
        ....
        ]
    }
    */

    /*
    if (!User.uid) {
        $location.path('/');
        alert("Please login first.");
    }
    */

    $scope.$on(Constants.user_logged_in, function() {
        $scope.isLoggedIn = true;
        $scope.loadAllConversations();
    });

    $scope.$on(Constants.user_logged_out, function() {
        $scope.isLoggedIn = false;
    });

    //Load all conversations
    $scope.loadAllConversations = function() {
        var ref = MessagingService.getAllConversations(User.uid);
        var i = 0;
        ref.on('child_added', function(conversation) {
            var promise = MessagingService.getConversationMetadata(conversation.key());
            promise.$loaded().then(function(metadata) {

                var read_status = "false";
                if (metadata.read_status && metadata.read_status[User.uid]) {
                    read_status = metadata.read_status[User.uid];
                }

                var conv = {
                    title: metadata.title,
                    read_status: read_status,
                    convID: conversation.key(),
                    arrayIndex: i
                };

                if (i == 0) {
                    $scope.loadSingleConversation(conv);
                }

                i++;

                var partners = [];
                angular.forEach(metadata.partners, function(partnerAttributes, partnerID) {
                    if (partnerAttributes.status === "ACTIVE") {
                        //load the partner
                        var partnerPromise = User.getUserByID(partnerID);
                        partnerPromise.$loaded().then(function(userData) {
                            partners.push({
                                id: partnerID,
                                name: userData.name,
                                type: userData.type,
                                image: userData.image
                            });
                        });
                    }
                    conv.partners = partners;
                });

                $scope.conversations.push(conv);
            });

        });
    }

    //add new message
    $scope._key_press = function(keyEvent) {
        if (keyEvent.keyCode != 13) {
            return;
        }

        keyEvent.preventDefault();

        $scope.isAddingMessage = true;

        MessagingService.addMessage($scope.conversation.convID, $scope.newMessage.data, User.uid)
            .then(function() {
                $scope.conversation.messages.push({
                    author: User.uid,
                    body: $scope.newMessage.data,
                    author_name: User.name,
                    author_image: User.image
                });

                $scope.newMessage.data = "";
                MessagingService.markConversationAsNotRead($scope.conversation.convID);
            }).catch(function(error) {
            console.log("error in commenting : " + error);
        }).finally(function() {
            $scope.isAddingMessage = false;
        });
    };

    //load selected single conversation
    $scope.loadSingleConversation = function(conv) {
        $scope.selectedConvIndex = conv.arrayIndex;

        var latestConvRef = MessagingService.getAllConversations(User.uid);
        latestConvRef.orderByKey().startAt(conv.convID + "").limitToFirst(1).once('child_added', function(conversation) {

            var conv = MessagingService.getConversationByID(conversation.key());
            $scope.conversation.convID = conversation.key();

            conv.$loaded().then(function(data) {
                $scope.conversation.messages = [];

                angular.forEach(data.messages, function(message) {
                    var authorPromise = User.getUserByID(message.author);
                    authorPromise.$loaded().then(function(authorData) {
                        $scope.conversation.messages.push({
                            author: message.author,
                            body: message.body,
                            author_name: authorData.name,
                            author_type: authorData.type,
                            author_image: authorData.image
                        });
                    });
                });

                MessagingService.markConversationAsRead($scope.conversation.convID);
            });

        });
    }


    //Load all conversations on manual click to the 'messages' button
    $scope.loadAllConversations();

    /*
    $scope.conversations = [
        {
            partner: "swebdev1",
            unread_count: 4,
            title: "Regarding opportunity"
        },
        {
            partner: "swebdev2",
            unread_count: 3,
            title: "Regarding another opportunity"
        },

    ];
    */
});
