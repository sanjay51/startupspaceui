'use strict';

startupSpaceApp.controller('CreatePostController',
                           function($scope, PostUtils, User, Validation, Utils, $location) {
    $scope.isCreatingPost = false;
    $scope.showWarning = false;
    $scope.warnMessage = "";
    $scope.createPostObject = {};
    $scope.createPostObject.title = "";
    $scope.createPostObject.description = "";

    $scope._click_cancel = function() {
        $location.path('/');
    };

    $scope.createPost = function() {
        if (! User.isLoggedIn()) {
            $('#loginModal').modal('show');
            return;
        }

        $scope.showWarning = false;
        $scope.isCreatingPost = true;
        var validationOutput = $scope.validateInput();

        if (validationOutput !== "") {
            $scope.showWarning = true;
            $scope.isCreatingPost = false;
            $scope.warnMessage = validationOutput;
            return;
        }

        var promise = PostUtils.createPost($scope.createPostObject.title, $scope.createPostObject.description, User.uid);

        promise.then(function(ref) {
            $location.path('/post/' + ref.key());
        }).catch(function(error) {
            $scope.warnMessage = Utils.getErrorMessage(error);
            $scope.showWarning = true;
        }).finally(function() {
            $scope.isCreatingPost = false;
        });
    };

    $scope.validateInput = function() {
        var message = "";
        message += Validation.validateTitle($scope.createPostObject.title);
        message += Validation.validateDescription($scope.createPostObject.description);

        return message;
    };
});
