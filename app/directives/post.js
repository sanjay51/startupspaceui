'use strict';

startupSpaceApp.directive('post', function() {
    return {
        restrict: 'E',
        templateUrl: 'directives/post.html',
        scope: {
            postData: '='
        },
        controller: 'SinglePostController'
    };
});
