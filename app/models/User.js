'use strict';

startupSpaceApp.factory("User", ["$firebaseObject", "Auth", "Constants", "BroadcastService",
    function($firebaseObject, Auth, Constants, BroadcastService) {
        var user = {};
        user.uid = null;
        user.object = null;
        user.email = "";
        user.name = "";
        user.type = "";
        user.image = "";
        user.is_logged_in = false;

        user.login = function(email, password) {
            return Auth.$authWithPassword({
                email: email,
                password: password
            });
        };

        user.logout = function() {
            Auth.$unauth();
            user.email = "";
            user.setLoggedIn(false);
            user.uid = null;
            user.object = null;
            BroadcastService.broadcast(Constants.user_logged_out);
        };

        user.getAuth = function() {
            return Auth.$getAuth();
        };

        user.signup = function(email, password) {
            return Auth.$createUser({
                email: email,
                password: password
            });
        };

        user.createProfile = function(name, type) {
            var userObj = user.getUser();
            userObj.profile = {};
            userObj.profile.public = {};
            userObj.profile.public.name = name;
            userObj.profile.public.type = type;

            return userObj.$save();
        };

        user.loadProfile = function() {
            var userObj = user.getUser();

            userObj.$loaded().then(function() {
                user.name = userObj.name;
                user.type = userObj.type;
                user.image = userObj.image;
            });
        };

        user.getUser = function() {
            if (user.uid === null) {
                return null;
            } else {
                if (user.object === null) {
                    var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/users/" + user.uid + "/profile/public");
                    user.object = $firebaseObject(ref);
                }

                return user.object;
            }
        };

        user.getUserByID = function(userID) {
            if (! userID) {
                return null;
            } else {
                var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com/users/" + userID + "/profile/public");
                return $firebaseObject(ref);
            }
        };

        user.setData = function(authData) {
            user.setUID(authData.uid);
            user.setProfileImage(authData.password.profileImageURL);
            user.setEmail(authData.password.email);
            user.setLoggedIn(true);
            BroadcastService.broadcast(Constants.user_logged_in);
        };

        user.setUID = function(uid) {
            user.uid = uid;
        };

        user.setEmail = function(email) {
            user.email = email;
        };

        user.setLoggedIn = function(is_logged_in) {
            user.is_logged_in = is_logged_in;
        };

        user.isLoggedIn = function() {
            return user.uid && user.is_logged_in;
        };

        user.setProfileImage = function(imageUrl) {
            user.image = imageUrl;
        };

        user.save = function() {
            user.object.$save().then(function(ref) {
                console.log("saved " + ref);
            }, function(error) {
                console.log(error);
            });
        };

        return user;
    }
]);
