startupSpaceApp.factory("Auth", ["$firebaseAuth",
    function($firebaseAuth) {
        var ref = new Firebase("https://glowing-inferno-3206.firebaseio.com");
        return $firebaseAuth(ref);
    }
]);
