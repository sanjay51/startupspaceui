var gulp = require('gulp');
var uglify = require('gulp-uglify');
var obfuscate = require('gulp-obfuscate');

gulp.task('build', function () {
  gulp.src('app/*.js')
  .pipe(uglify())
  .pipe(obfuscate())
  .pipe(gulp.dest('dist'));

  gulp.src('app/**/*.html')
  .pipe(gulp.dest('dist'));
});
